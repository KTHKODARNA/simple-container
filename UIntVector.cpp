#include <iostream>
#include <stdexcept>
//#include "UIntVector.hpp"

class UIntVector {
public:
	unsigned int* v = NULL;
	int size = 0;
	UIntVector() {
	    std::cout << "default-constructor" << std::endl;
	}
	UIntVector(const std::size_t &n) {
		std::cout << "size_t-constructor" << std::endl;
		v = new unsigned int[n];
		size = n;
		for(int i=0; i<n; i++)
			v[i] = 0;
	}
	~UIntVector() {
		std::cout << "destructor" << std::endl;
		if(v != NULL) 
			delete [] v;
	}
	UIntVector(std::initializer_list<int> args) {
		std::cout << "initializer_list-constructor" << std::endl;
		int s = args.size();
		v = new unsigned int[s];
		std::initializer_list<int>::iterator it;  // same as: const int* it
		int i = 0;
		for (it=args.begin(); it != args.end(); ++it) {
			v[i] = *it;
			i++;
		}
	}
	UIntVector(UIntVector &&vec) {
		std::cout << "move-constructor" << std::endl;
		size = vec.size;
		v = vec.v;

		vec.size = 0;
		vec.v = NULL;

	}
	UIntVector(const UIntVector &vec) {
		std::cout << "copy-constructor" << std::endl;
		v = new unsigned int[vec.size];
		for(int i=0; i<vec.size; i++) {
			v[i] = vec.v[i];
		}
	}

	unsigned int operator[] (int n) const {
		return v[n];
	}

	unsigned int & operator[] (int n) {
		return v[n];
	}

	UIntVector& operator= (UIntVector vec) {
		size = vec.size;
		v = vec.v;

		vec.size = 0;
		vec.v = NULL;

		return *this;
	}

	UIntVector& operator= (UIntVector&& vec) {
		size = vec.size;
		v = vec.v;

		vec.size = 0;
		vec.v = NULL;

		return *this;
	}



};

int main() {

	UIntVector u {1,2,3,4};
	u[3] = 2;
	std::cout << u[3] << std::endl;
	// UIntVector original = UIntVector(5);
	// original.v[3] = 21;
	// UIntVector cp = original;
	// cp.v[3] = 40;

	// UIntVector a(7);           // initiering med 7 element
 //    UIntVector b(a);           // kopieringskonstruktor 
 //    UIntVector c = a;          // kopieringskonstruktor 


	return 0;
}