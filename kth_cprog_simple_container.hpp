#include <iostream>
#include <stdexcept>

class UIntVector {
public:
	unsigned int* v;
	std::size_t length;
	UIntVector() {
		v = new unsigned int[0];
		length = 0;
		std::clog << "default-constructor" << std::endl;

	}
	explicit UIntVector(const std::size_t &n) {
		std::clog << "size_t-constructor" << std::endl;
		v = new unsigned int[n];
		length = n;
		for(int i=0; i<n; i++)
			v[i] = 0;
	}
	~UIntVector() {
		std::clog << "destructor" << std::endl;
		if(v != NULL) {
			delete [] v;
			//delete v;
		}
	}
	UIntVector(std::initializer_list<unsigned int> args) {
		length = args.size();
		std::clog << "initializer_list-constructor" << std::endl;
		v = new unsigned int[length];
		std::initializer_list<unsigned int>::iterator it;  // same as: const int* it
		int i = 0;
		for (it=args.begin(); it != args.end(); ++it) {
			v[i] = *it;
			i++;
		}
	}
	UIntVector(UIntVector &&vec) {
		std::clog << "move-constructor" << std::endl;
		length = vec.length;
		v = vec.v;

		vec.length = 0;
		//TODO: hantera gamla pekaren.
		vec.reset();
		vec.v = NULL;
	}
	UIntVector(const UIntVector &vec) {
		std::clog << "copy-constructor" << std::endl;
		length = vec.length;

		v = new unsigned int[length];
		for(int i=0; i<length; i++) {
			v[i] = vec[i];
		}
	}

	unsigned int& operator[] (const int n) {
		if(n < 0 || n >= length) {
			throw std::out_of_range("Index out of bounds");
		}
		return v[n];
	}

	const unsigned int& operator[] (const int n) const {
		if(n < 0 || n >= length) {
			throw std::out_of_range("Index out of bounds");
		}
		return v[n];
	}

	/**
	 * Copy assignment operator
	 */
	UIntVector& operator= (const UIntVector& vec) {
		if(v == vec.v) {
			return *this;
		}
		std::clog << "copy-assignment" << std::endl;
		length = vec.length;
		if(v != NULL)
			delete [] v;
		v = new unsigned int[length]; //funkar utan detta också...

		for(int i=0; i<length; i++) {
			v[i] = vec[i];
		}

		return *this;
	}

	/**
	 * Move assignment operator
	 */
	UIntVector& operator= (UIntVector&& vec) {
		if(v == vec.v) {
			return *this;
		}
		std::clog << "move-assignment" << std::endl;
		length = vec.length;
		if(v != NULL)
			delete [] v;
		v = vec.v;

		vec.length = 0;
		vec.reset();
		vec.v = NULL;

		return *this;
	}

	void reset() {
		for(int i = 0; i < length; i++)
			v[i] = {};
	}

	std::size_t size() const {
		return length;
	}

};

